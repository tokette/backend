<?php

namespace App\DataFixtures;

use App\Entity\Project;
use App\Entity\Team;
use App\Entity\User\Adviser;
use App\Entity\User\BaseUser;
use App\Entity\User\Student;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

class AppFixtures extends Fixture
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    public function __construct(ManagerRegistry $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Creates fixtures and load them into the database.
     *
     * @param ObjectManager $manager
     *
     * @throws Exception
     */
    public function load(ObjectManager $manager)
    {
        $this->manager->getConnection()->exec('ALTER TABLE base_user AUTO_INCREMENT = 1;');
        $this->manager->getConnection()->exec('ALTER TABLE project AUTO_INCREMENT = 1;');
        $this->manager->getConnection()->exec('ALTER TABLE team AUTO_INCREMENT = 1;');

        $student = $this->createStudent('student@mail.com', 'foo', 'John', 'Doe');
        $adviser = $this->createAdviser('adviser@mail.com', 'bar', 'Jane', 'Doe', 0);

        $project = $this->createProject('réparer le réseau de l\'EPSI', 100);
        $team = $this->createTeam('coquette', $project, [$student]);

        $manager->persist($student);
        $manager->persist($adviser);
        $manager->persist($project);
        $manager->persist($team);
        $manager->flush();
    }

    /**
     * Creates a team from a student array.
     *
     * @param string  $name
     * @param Project $project
     * @param array   $students
     *
     * @return Team
     */
    private function createTeam(string $name, Project $project, array $students)
    {
        $team = new Team();
        $team->setName($name);
        $team->setProject($project);
        $team->setToken($project->getDefaultToken());
        foreach ($students as $student) {
            $team->addStudent($student);
        }

        return $team;
    }

    /**
     * Creates a project.
     *
     * @param string $name
     * @param int    $defaultToken
     *
     * @return Project
     */
    private function createProject(string $name, int $defaultToken)
    {
        $project = new Project();
        $project->setName($name);
        $project->setDefaultToken($defaultToken);

        return $project;
    }

    /**
     * Creates a student.
     *
     * @param string $email
     * @param string $apiKey
     * @param string $forename
     * @param string $surname
     *
     * @return Student
     */
    private function createStudent(string $email, string $apiKey, string $forename, string $surname)
    {
        $student = $this->createUser(new Student(), $email, $apiKey, $forename, $surname);

        return $student;
    }

    /**
     * Creates an adviser.
     *
     * @param string $email
     * @param string $apiKey
     * @param string $forename
     * @param string $surname
     * @param int    $token
     *
     * @return Student
     */
    private function createAdviser(string $email, string $apiKey, string $forename, string $surname, int $token)
    {
        $adviser = $this->createUser(new Adviser(), $email, $apiKey, $forename, $surname);
        $adviser->setToken($token);

        return $adviser;
    }

    /**
     * Creates a BaseUser.
     *
     * @param BaseUser $user
     * @param string   $email
     * @param string   $apiKey
     * @param string   $forename
     * @param string   $surname
     *
     * @return mixed
     */
    private function createUser(BaseUser $user, string $email, string $apiKey, string $forename, string $surname)
    {
        $user->setEmail($email);
        $user->setApiKey($apiKey);
        $user->setForename($forename);
        $user->setSurname($surname);

        return $user;
    }
}
