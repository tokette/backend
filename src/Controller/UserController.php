<?php

namespace App\Controller;

use App\Entity\User\BaseUser;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/api")
 */
class UserController extends Controller
{
    /**
     * @Route("/user", name="user_login")
     * @Method({"GET"})
     *
     * @param Request             $request
     * @param SerializerInterface $serializer
     *
     * @return Response
     */
    public function getBaseUserAction(Request $request, SerializerInterface $serializer)
    {
        $apiKey = $request->headers->get('X-AUTH-TOKEN');

        $repo = $this->getDoctrine()->getRepository(BaseUser::class);
        $user = $repo->findOneBy(['apiKey' => $apiKey]);
        $teams = [];
        foreach ($user->getTeams() as $team) {
            $teams[] = [
                'id' => $team->getId(),
                'name' => $team->getName(),
                'project' => $team->getProject()->getId(),
                'token' => $team->getToken(),
            ];
        }
        $arrayUser = [
            'teams' => $teams,
            'email' => $user->getEmail(),
            'forename' => $user->getForename(),
            'surname' => $user->getSurname(),
        ];
        $serializedUser = $serializer->serialize($arrayUser, 'json');

        return new Response($serializedUser);
    }
}
