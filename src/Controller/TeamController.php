<?php

namespace App\Controller;

use App\Entity\Project;
use App\Entity\Team;
use App\Entity\User\Student;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/api/team")
 */
class TeamController extends Controller
{
    /**
     * @Route("/{id}", name="get_team")
     * @Method({"GET"})
     *
     * @param Request             $request
     * @param SerializerInterface $serializer
     *
     * @return Response
     */
    public function getTeamAction(SerializerInterface $serializer, $id)
    {
        $repo = $this->getDoctrine()->getRepository(Team::class);
        $team = $repo->find($id);
        $students = [];
        foreach ($team->getStudents() as $student) {
            $students[] = [
                'id' => $student->getId(),
                'forename' => $student->getForename(),
                'surname' => $student->getSurname(),
            ];
        }

        $arrayTeam = [
            'id' => $team->getId(),
            'students' => $students,
            'token' => $team->getToken(),
            'project' => $team->getProject()->getId(),
        ];
        $serializedUser = $serializer->serialize($arrayTeam, 'json');

        return new Response($serializedUser);
    }

    /**
     * @Route("/{id}/user", name="add_user_to_team")
     * @Method({"GET"})
     *
     * @param Request $request
     * @param $id
     * @param EntityManager $em
     *
     * @return Response
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addStudentToTeam(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $apiKey = $request->headers->get('X-AUTH-TOKEN');

        $team = $this->getDoctrine()->getRepository(Team::class)->find($id);
        $student = $this->getDoctrine()->getRepository(Student::class)->findOneBy(['apiKey' => $apiKey]);
        $team->addStudent($student);
        $student->addTeam($team);

        $em->persist($team);
        $em->persist($student);
        $em->flush();

        return new JsonResponse(['message' => 'Student added'], 200);
    }

    /**
     * @Route("/{id}/user/{userId}", name="delete_user_to_team")
     * @Method({"DELETE"})
     *
     * @param Request $request
     * @param $id
     * @param EntityManager $em
     *
     * @return Response
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteStudentToTeam($id, $userId)
    {
        $em = $this->getDoctrine()->getManager();

        $team = $this->getDoctrine()->getRepository(Team::class)->find($id);
        $student = $this->getDoctrine()->getRepository(Student::class)->find($userId);
        $team->removeStudent($student);
        $student->removeTeam($team);

        $em->persist($team);
        $em->persist($student);
        $em->flush();

        return new JsonResponse(['message' => 'Student deleted'], 200);
    }

    /**
     * @Route("/project/{id}", name="create_team")
     * @Method({"POST"})
     *
     * @param Request $request
     * @param $id
     * @return Response
     *
     */
    public function createTeam(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $apiKey = $request->headers->get('X-AUTH-TOKEN');
        $name = $request->request->get('name');

        $project = $this->getDoctrine()->getRepository(Project::class)->find($id);
        $student = $this->getDoctrine()->getRepository(Student::class)->findOneBy(array("apiKey" => $apiKey));

        $team = new Team();

        $team->setProject($project);
        $team->setName($name);
        $team->addStudent($student);
        $team->setToken($project->getDefaultToken());

        $em->persist($team);
        $em->persist($student);
        $em->persist($project);
        $em->flush();

        return new JsonResponse(['message' => 'Team created'], 200);
    }
}
