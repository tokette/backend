<?php

namespace App\Controller;

use App\Entity\Team;
use App\Entity\Transaction;
use App\Entity\User\Adviser;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/api")
 */
class TransactionController extends Controller
{
    /**
     * @Method({"POST"})
     * @Route("/transaction/open/{adviser_id}/{team_id}")
     * @ParamConverter("adviser", options={"id" = "adviser_id"})
     * @ParamConverter("team", options={"id" = "team_id"})
     *
     * @param Adviser $adviser
     * @param Team    $team
     *
     * @return Response
     */
    public function openTransactionAction(Adviser $adviser, Team $team)
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository(Transaction::class);

        $exists = $repo->findOneBy(['adviser' => $adviser, 'status' => Transaction::STATUS_OPENED]);

        if ($exists) {
            return new JsonResponse(['message' => 'adviser has already an opened transaction'], 403);
        }

        $transaction = new Transaction();
        $transaction->setAdviser($adviser);
        $transaction->setTeam($team);
        $transaction->setStatus(Transaction::STATUS_PENDING);

        return new JsonResponse(['message' => 'transaction created'], 201);
    }
}
