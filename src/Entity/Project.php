<?php

namespace App\Entity;

use App\Entity\User\Adviser;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class Project
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", unique=true)
     */
    private $name;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Team", mappedBy="project")
     */
    private $teams;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\User\Adviser", mappedBy="projects")
     */
    private $advisers;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $defaultToken;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    protected $startAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $endAt;

    public function __construct()
    {
        $this->startAt = new \DateTime();
        $this->teams = new ArrayCollection();
        $this->advisers = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return ArrayCollection
     */
    public function getTeams(): Collection
    {
        return $this->teams;
    }

    /**
     * @param ArrayCollection $teams
     */
    public function setTeams(ArrayCollection $teams)
    {
        $this->teams = $teams;
    }

    /**
     * @param Team $team
     *
     * @return Project
     */
    public function addTeam(Team $team)
    {
        $this->teams[] = $team;
        $team->setProject($this);

        return $this;
    }

    /**
     * @param Team $team
     */
    public function removeTeam(Team $team)
    {
        $this->teams->removeElement($team);
    }

    /**
     * @return ArrayCollection
     */
    public function getAdvisers(): Collection
    {
        return $this->advisers;
    }

    /**
     * @param ArrayCollection $advisers
     */
    public function setAdvisers(ArrayCollection $advisers)
    {
        $this->advisers = $advisers;
    }

    /**
     * @param Adviser $adviser
     *
     * @return Project
     */
    public function addAdviser(Adviser $adviser)
    {
        $this->advisers[] = $adviser;
        $adviser->setProjects($this);

        return $this;
    }

    /**
     * @param Adviser $adviser
     */
    public function removeAdviser(Adviser $adviser)
    {
        $this->advisers->removeElement($adviser);
    }

    /**
     * @return int
     */
    public function getDefaultToken(): ?int
    {
        return $this->defaultToken;
    }

    /**
     * @param int $defaultToken
     */
    public function setDefaultToken(int $defaultToken)
    {
        $this->defaultToken = $defaultToken;
    }

    /**
     * @return \DateTime
     */
    public function getStartAt(): ?\DateTime
    {
        return $this->startAt;
    }

    /**
     * @param \DateTime $startAt
     */
    public function setStartAt(\DateTime $startAt)
    {
        $this->startAt = $startAt;
    }

    /**
     * @return \DateTime
     */
    public function getEndAt(): ?\DateTime
    {
        return $this->endAt;
    }

    /**
     * @param \DateTime $endAt
     */
    public function setEndAt(\DateTime $endAt)
    {
        $this->endAt = $endAt;
    }
}
