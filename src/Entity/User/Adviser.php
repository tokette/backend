<?php

namespace App\Entity\User;

use App\Entity\Project;
use App\Entity\Transaction;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class Adviser extends BaseUser
{
    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Project", inversedBy="advisers")
     */
    private $projects;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Transaction", mappedBy="adviser")
     */
    private $transactions;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $token;

    /**
     * Adviser constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->token = 0;
        $this->projects = new ArrayCollection();
        $this->transactions = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getUsername();
    }

    /**
     * @return ArrayCollection
     */
    public function getProjects(): Collection
    {
        return $this->projects;
    }

    /**
     * @param ArrayCollection $projects
     */
    public function setProjects(ArrayCollection $projects)
    {
        $this->projects = $projects;
    }

    /**
     * @param Project $project
     *
     * @return Adviser
     */
    public function addProject(Project $project)
    {
        $this->projects[] = $project;
        $project->addAdviser($this);

        return $this;
    }

    /**
     * @param Project $project
     */
    public function removeProject(Project $project)
    {
        $this->projects->removeElement($project);
    }

    /**
     * @return ArrayCollection
     */
    public function getTransactions(): Collection
    {
        return $this->transactions;
    }

    /**
     * @param ArrayCollection $transactions
     */
    public function setTransactions(ArrayCollection $transactions)
    {
        $this->transactions = $transactions;
    }

    /**
     * @param Transaction $transaction
     *
     * @return Adviser
     */
    public function addTransaction(Transaction $transaction)
    {
        $this->transactions[] = $transaction;
        $transaction->setAdviser($this);

        return $this;
    }

    /**
     * @param Transaction $transaction
     */
    public function removeTransaction(Transaction $transaction)
    {
        $this->transactions->removeElement($transaction);
    }

    /**
     * @return int
     */
    public function getToken(): ?int
    {
        return $this->token;
    }

    /**
     * @param int $token
     */
    public function setToken(int $token)
    {
        $this->token = $token;
    }
}
