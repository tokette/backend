<?php

namespace App\Entity;

use App\Entity\User\Adviser;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity()
 */
class Transaction
{
    const STATUS_OPENED = 'opened';
    const STATUS_REFUSED = 'refused';
    const STATUS_PENDING = 'pending';

    use TimestampableEntity;

    /**
     * @var int.
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Adviser
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User\Adviser", inversedBy="transactions")
     */
    private $adviser;

    /**
     * @var Team
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Team", inversedBy="transactions")
     */
    private $team;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $price;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false)
     */
    protected $status;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="start")
     * @ORM\Column(type="datetime")
     */
    protected $startedAt;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="end")
     * @ORM\Column(type="datetime")
     */
    protected $endedAt;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @param int $price
     */
    public function setPrice(int $price): void
    {
        $this->price = $price;
    }

    /**
     * Returns createdAt.
     *
     * @return \DateTime
     */
    public function getStartedAt()
    {
        return $this->startedAt;
    }

    /**
     * @param \DateTime $startedAt
     *
     * @return $this
     */
    public function setStartedAt(\DateTime $startedAt)
    {
        $this->startedAt = $startedAt;

        return $this;
    }

    /**
     * Returns createdAt.
     *
     * @return \DateTime
     */
    public function getEndedAt()
    {
        return $this->endedAt;
    }

    /**
     * @param \DateTime $ended
     *
     * @return $this
     */
    public function setEndedAt(\DateTime $ended)
    {
        $this->endedAt = $ended;

        return $this;
    }

    /**
     * @return Team
     */
    public function getTeam(): Team
    {
        return $this->team;
    }

    /**
     * @param Team $team
     */
    public function setTeam(Team $team): void
    {
        $this->team = $team;
    }

    /**
     * @return Adviser
     */
    public function getAdviser(): Adviser
    {
        return $this->adviser;
    }

    /**
     * @param Adviser $adviser
     */
    public function setAdviser(Adviser $adviser)
    {
        $this->adviser = $adviser;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status)
    {
        $this->status = $status;
    }
}
