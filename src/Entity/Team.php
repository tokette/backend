<?php

namespace App\Entity;

use App\Entity\User\Student;
use App\Entity\User\Transaction;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class Team
{
    /**
     * @var int.
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", unique=true)
     */
    private $name;

    /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Project", inversedBy="teams")
     */
    private $project;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\User\Student", mappedBy="teams")
     */
    private $students;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\User\Student", mappedBy="team")
     */
    private $transactions;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $token;

    /**
     * @param Project $project
     */
    public function __construct()
    {
        $this->students = new ArrayCollection();
        $this->transactions = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return Project
     */
    public function getProject(): ?Project
    {
        return $this->project;
    }

    /**
     * @param Project $project
     */
    public function setProject(Project $project)
    {
        $this->project = $project;
    }

    /**
     * @return ArrayCollection
     */
    public function getStudents(): Collection
    {
        return $this->students;
    }

    /**
     * @param ArrayCollection $students
     */
    public function setStudents(ArrayCollection $students)
    {
        $this->students = $students;
    }

    /**
     * @param Student $student
     */
    public function addStudent(Student $student)
    {
        if ($this->students->contains($student)) {
            return;
        }

        $this->students[] = $student;
        $student->addTeam($this);
    }

    /**
     * @param Student $student
     */
    public function removeStudent(Student $student)
    {
        $this->students->removeElement($student);
    }

    /**
     * @return ArrayCollection
     */
    public function getTransactions(): Collection
    {
        return $this->transactions;
    }

    /**
     * @param ArrayCollection $transactions
     */
    public function setTransactions(ArrayCollection $transactions)
    {
        $this->transactions = $transactions;
    }

    /**
     * @param Transaction $transaction
     *
     * @return Team
     */
    public function addTransaction(Transaction $transaction)
    {
        $this->transactions[] = $transaction;
        $transaction->setTeam($this);

        return $this;
    }

    /**
     * @param Transaction $transaction
     */
    public function removeTransaction(Transaction $transaction)
    {
        $this->transactions->removeElement($transaction);
    }

    /**
     * @return int
     */
    public function getToken(): ?int
    {
        return $this->token;
    }

    /**
     * @param int $token
     */
    public function setToken(int $token)
    {
        $this->token = $token;
    }
}
