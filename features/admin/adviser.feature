Feature:
  In order to manage my advisers
  As an admin
  I want to be able to create, edit and delete them

  Scenario: I can see my advisers
    Given I am on "/admin/?action=list&entity=Adviser"
    Then I should see "Liste des intervenants"
    And I should see "adviser@mail.com"

  Scenario: I can create a student
    Given I am on "/admin/?action=new&entity=Adviser"
    Then I should see "Create Adviser"
    When I fill in "adviser_email" with "celestin@mail.com"
    And I fill in "adviser_apiKey" with "foobar"
    And I fill in "adviser_forename" with "celestin"
    And I fill in "adviser_surname" with "doe"
    And I click the "button.action-save" element
    Then I should be on "/admin/?action=list&entity=Adviser"
    And I should see "celestin@mail.com"
