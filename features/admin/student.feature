Feature:
  In order to manage my students
  As an admin
  I want to be able to create, edit and delete them

  Scenario: I can see my students
    Given I am on "/admin/?action=list&entity=Student"
    Then I should see "Liste des étudiants"
    And I should see "student@mail.com"

  Scenario: I can create a student
    Given I am on "/admin/?action=new&entity=Student"
    Then I should see "Create Student"
    When I fill in "student_email" with "denis@mail.com"
    And I fill in "student_apiKey" with "foobar"
    And I fill in "student_forename" with "denis"
    And I fill in "student_surname" with "doe"
    And I click the "button.action-save" element
    Then I should be on "/admin/?action=list&entity=Student"
    And I should see "denis@mail.com"
