Feature:
  In order to access the API
  As a student or an adviser
  I want to be able to log into the API

  Scenario: I can access the API with a valid token
    Given I add "X-AUTH-TOKEN" header equal to "foo"
    When I send a GET request to "/api/user"
    Then the response status code should be 200
    And the response should contain "\"forename\":\"John\""
    And the response should contain "\"forename\":\"John\""
    And the response should contain "\"surname\":\"Doe\""