Feature:
  In order to access the API
  As a student or an adviser
  I want to be able to log into the API

  Scenario: I can't access the API without a token
    When I send a "GET" request to "/api/user"
    Then the response status code should be 401
    And the response should be equal to:
    """
    {"message":"authentication required"}
    """

  Scenario: I can't access the API with a wrong token
    Given I add "X-AUTH-TOKEN" header equal to "dummy"
    When I send a GET request to "/api/user"
    Then the response status code should be 403
    And the response should be equal to:
    """
    {"message":"api key verification failed"}
    """


  Scenario: I can access the API with a valid token
    Given I add "X-AUTH-TOKEN" header equal to "foo"
    When I send a GET request to "/api/user"
    Then the response status code should be 200