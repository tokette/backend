Feature:
  In order to perform transactions
  As a student or an adviser
  I need api routes to manage them

  Scenario: I can open a transaction with a free adviser
    Given I add "X-AUTH-TOKEN" header equal to "foo"
    When I send a POST request to "/api/transaction/open/1/1"
    Then the response status code should be 201